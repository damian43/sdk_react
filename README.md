
# react-native-devsar-android

## Getting started

`$ npm install react-native-devsar-android --save`

### Mostly automatic installation

`$ react-native link react-native-devsar-android`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-devsar-android` and add `RNDevsarAndroid.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNDevsarAndroid.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNDevsarAndroidPackage;` to the imports at the top of the file
  - Add `new RNDevsarAndroidPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-devsar-android'
  	project(':react-native-devsar-android').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-devsar-android/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-devsar-android')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNDevsarAndroid.sln` in `node_modules/react-native-devsar-android/windows/RNDevsarAndroid.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Devsar.Android.RNDevsarAndroid;` to the usings at the top of the file
  - Add `new RNDevsarAndroidPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNDevsarAndroid from 'react-native-devsar-android';

// TODO: What to do with the module?
RNDevsarAndroid;
```
  