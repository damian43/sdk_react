using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Devsar.Android.RNDevsarAndroid
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNDevsarAndroidModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNDevsarAndroidModule"/>.
        /// </summary>
        internal RNDevsarAndroidModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNDevsarAndroid";
            }
        }
    }
}
