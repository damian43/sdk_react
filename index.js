
import { NativeModules } from 'react-native';

const { ToastModule, PermissionModule } = NativeModules;

export { ToastModule, PermissionModule };