package com.devsar.react

import com.facebook.react.ReactPackage
import com.facebook.react.bridge.JavaScriptModule
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager
import java.util.*

class DevsarUiPackage : ReactPackage {

    override fun createJSModules(): MutableList<Class<out JavaScriptModule>> {
        return Collections.emptyList()
    }

    override fun createNativeModules(reactContext: ReactApplicationContext?): MutableList<NativeModule> {
        return Collections.emptyList()
    }

    override fun createViewManagers(reactContext: ReactApplicationContext?): List<ViewManager<*, *>> {
        return mutableListOf(
                TitleTextViewManager()
        )
    }
}