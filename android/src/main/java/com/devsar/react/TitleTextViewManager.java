package com.devsar.react;

import com.devsar.android.view.TitleTextView;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

public class TitleTextViewManager extends SimpleViewManager<TitleTextView> {

    public static final String REACT_CLASS = "TitleTextView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected TitleTextView createViewInstance(ThemedReactContext reactContext) {
        return new TitleTextView(reactContext);
    }

    @ReactProp(name = "text")
    public void setText(TitleTextView titleTextView, String text) {
        titleTextView.setText(text);
    }
}