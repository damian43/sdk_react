package com.devsar.react.permission

import com.devsar.android.permission.Permission
//import com.devsar.react.Event
import com.facebook.react.bridge.*
import java.util.*
import kotlin.collections.ArrayList

class PermissionModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {

    override fun getConstants(): Map<String, Int>? {
        val constants: MutableMap<String, Int> = HashMap()
        constants["GRANTED"] = Permission.GRANTED
        constants["DENIED"] = Permission.DENIED
        constants["NEVER_ASK_AGAIN"] = Permission.NEVER_ASK_AGAIN
        return constants
    }

    override fun getName(): String {
        return "PermissionModule"
    }

    /**
     * Return state of permission
     * @param permission Permission
     * @param callback return Int (code state)
     **/
    @ReactMethod
    fun getState(permission: String, callback: Callback) {
        callback.invoke(Permission().getState(currentActivity!!, reactApplicationContext, permission))
    }

    /**
     * Return state of permissions
     * @param array Permissions list
     * @param callback return Map with states of permissions
     **/
    @ReactMethod
    fun getStates(array: ReadableArray, callback: Callback) {
        val permissions = ArrayList<String>()
        for (i in 0 until array.size()) {
            permissions.add(array.getString(i))
        }
        val states = Permission().getStates(currentActivity!!, reactApplicationContext, permissions)
        val output = Arguments.createMap()
        for (i in states) {
            output.putInt(i.key, i.value)
        }
        callback.invoke(output)
    }

    /**
     * Check if permission is granted
     * @param permission Permission
     * @param callback return boolean isGranted
     **/
    @ReactMethod
    fun isGranted(permission: String, callback: Callback) {
        callback.invoke(Permission().isGranted(reactApplicationContext, permission))
    }

    /**
     * Check if permissions are granted
     * @param array Permissions list
     * @param callback return boolean areGranted
     **/
    @ReactMethod
    fun areGranted(array: ReadableArray, callback: Callback) {
        val permissions = ArrayList<String>()
        for (i in 0 until array.size()) {
            permissions.add(array.getString(i))
        }
        callback.invoke(Permission().areGranted(reactApplicationContext, permissions))
    }

    /**
     * Returns if justification for request permission must be shown
     * @param permission Permission
     * @param callback return boolean if should show request permission rationale
     **/
    @ReactMethod
    fun shouldShowRequestPermissionRationale(permission: String, callback: Callback) {
        callback.invoke(Permission().shouldShowRequestPermissionRationale(currentActivity!!, permission))
    }

    /**
     * Request a permission
     * @param permission Permissions list
     * @param callback action to invoke when permission has been requested (boolean is granted)
     **/
    @ReactMethod
    fun requestPermission(permission: String, callback: Callback) {
        Permission().requestPermission(reactApplicationContext, permission, action = {
            callback.invoke(it)
        })
    }

    /**
     * Request multiple permissions
     * @param array Permissions list
     * @param callback action to invoke when permissions has been requested (Map <Permission String,Granted Boolean>)
     **/
    @ReactMethod
    fun requestPermissions(array: ReadableArray, callback: Callback) {
        val permissions = ArrayList<String>()
        for (i in 0 until array.size()) {
            permissions.add(array.getString(i))
        }
        Permission().requestPermissions(reactApplicationContext, permissions.toList(), action = {
            val output = Arguments.createMap()
            for (i in it) {
                output.putBoolean(i.key, i.value)
            }
            callback.invoke(output)
        })
    }

    //quitar este metodo TODO
    /*@ReactMethod
    fun test() {
        //TODO test events
        val params = Arguments.createMap()
        params.putString("pulsaciones", "150")
        Event.sendEvent(reactApplicationContext, "EventPulso", params)
    }*/
}