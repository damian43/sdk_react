package com.devsar.react

import com.devsar.react.permission.PermissionModule
import com.devsar.react.toast.ToastModule
import com.facebook.react.ReactPackage
import com.facebook.react.bridge.JavaScriptModule
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager
import java.util.*

class DevsarPackage : ReactPackage {

    override fun createJSModules(): MutableList<Class<out JavaScriptModule>> {
        return Collections.emptyList()
    }

    override fun createNativeModules(reactContext: ReactApplicationContext?): MutableList<NativeModule> {
        return mutableListOf(
                ToastModule(reactContext!!),
                PermissionModule(reactContext)
        )
    }

    override fun createViewManagers(reactContext: ReactApplicationContext?): List<ViewManager<*, *>> {
        return Collections.emptyList()
    }
}